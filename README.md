# antitheme
A minimalist theme for CVs and personal pages. The idea behind it is to separate
relatively structured info (contact info, work history) to be more yaml-driven
while more unstructured content is more markdown heavy.
