# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "antitheme"
  spec.version       = "0.3.1"
  spec.authors       = ["zluudg"]
  spec.email         = ["zluudg@zluudg.com"]

  spec.summary       = %q{Attempted minimalistic Jekyll theme.}
  spec.homepage      = "https://gitlab.com/zluudg/antitheme"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_data|_layouts|_includes|_sass|LICENSE|README|_config\.yml)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.3"
end
